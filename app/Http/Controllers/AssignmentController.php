<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AssignmentController extends Controller
{
    public function index()
    {
        return view('backend.assignment');
    }

   public function addassignment()
    {
        return view('backend.addassignment');
    }

    public function reportdispute()
    {
        return view('backend.reportdispute');
    }

    public function viewassignment()
    {
        return view('backend.viewassignment');
    }

    public function editassignment()
    {
        return view('backend.editassignment');
    }



}
