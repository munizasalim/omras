@extends('admin')

  
@section('admincontent')
    <div class="page">
        <!-- Page Content -->
      <div class="page-content">

        <div class="panel">
                <div class="example-wrap">
                  <header class="panel-heading">
                <h3 class="panel-title">
                 Update Status
                </h3>
              </header>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-9">

                <!-- Example Basic Form (Form grid) -->
                  
                  <div class="example">
                    <form action="{{url('test')}}" method="post">
                    {{ csrf_field() }} 
                    <div class="panel-body container-fluid">
                              <div class="row row-lg"> 
                                <div class="col-md-6 col-lg-4">
                                  <div class="example-wrap">
                                    <h4 class="example-title">Assignment No:</h4>
                                  <input type="text" name="AssignmentNo" class="form-control" id="inputPlaceholder" placeholder="1" disabled> 
                                  </div>  
                            </div>

                              <div class="col-md-6 col-lg-4">
                                <div class="example-wrap">
                                    <h4 class="example-title">Event Name:</h4>
                                  <input type="text" name="EventName"  class="form-control" id="inputPlaceholder" placeholder="PM Event" disabled> 
                                  </div>  
                              </div>
                            </div>
                          </div>
                              
                                <div class="panel-body container-fluid">
                                    <div class="row row-lg">
                                     <div class="col-md-6 col-lg-4">
                                      <div class="example-wrap">
                                        <h4 class="example-title">Number Of Clips Added</h4>
                                        <input type="Number" name="NumberOfClipsAdded" class="form-control" id="inputRounded" min="0" placeholder="Add Number of Clips">
                                      </div>  
                                  </div>

                                <div class="col-md-4 form-group">      
                                    <h4 class="example-title">Duration Of Clips</h4>
                                    <div class="col-md-6 form-group">
                                        <input type="Number" class="form-control" placeholder="Hours" min="0" name="hours">
                                      </div>
                                    <div class="col-md-6 form-group">
                                        <input type="Number" class="form-control" placeholder="Minutes" min="0" name="minutes" >
                                      </div>
                                      <div class="col-md-6 form-group">
                                        <input type="Number" class="form-control" placeholder="Seconds" min="0" name="seconds">
                                      </div>
                                  </div>


                                <div class="col-md-6 col-lg-4">  
                                      <div class="example-wrap">
                                        <h4 class="example-title">Size : <small>(Please Mention Gbs or MBs)</small></h4>
                                        <input type="text" name="SizeOfClipsAdded" class="form-control" id="inputRounded" placeholder="Size of all Clips">
                                      </div>
                                  </div>
                              </div>
                        </div>
                      

                      <div class="panel-body container-fluid">
                                    <div class="row row-lg">
                                     <div class="col-md-6 col-lg-4">
                                      <div class="example-wrap">
                                        <h4 class="example-title">Start Grab Time</h4>
                                        <input type="time"  name="StartGrabTime" class="form-control" id="inputRounded">
                                      </div>  
                                  </div>

                                <div class="col-md-6 col-lg-4">
                                  <div class="example-wrap">
                                    <h4 class="example-title">End Grab Time</h4>
                                    <input type="time" name="EndGrabTime" class="form-control" id="appt" name="appt" >
                                  </div>  
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="panel-body container-fluid">
                                  <div class="col-md-8 col-lg-8">
                                      <div class="example-wrap">
                                        <h4 class="example-title">Comment</h4>
                                        <textarea class="form-control" id="textareaDefault" name="comments" rows="3"></textarea>
                                      </div>
                                    </div>
                              </div>

                                <div class="panel-body container-fluid">
                                   
                                  <div class="col-md-6 col-lg-4"> 
                                      <div class="example-wrap">
                                        <button type="submit" class="btn btn-primary">Submit </button>
                                        <a href="{{url('reporter')}}" class="btn btn-danger">Back </a>
                                      </div>
                                    
                                </div>
                             </div>   
                          </div>
        
                      </form>
                    </div> 
                </div>
              </div>
            </div>
    <!-- End Page Content -->
     </div>
@endsection
