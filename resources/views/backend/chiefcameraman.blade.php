@extends('admin')

  
@section('admincontent')

<div class="page">
	<div class="page-content">
		<div class="panel">
			<div class="panel-body">
				<header class="panel-heading">
                	<h3 class="panel-title">
                  		Assignment Info
                	</h3>
              	</header>
            <div class="example table-responsive">
              <table class="table table-striped table-bordered" data-plugin="floatThead">
                <thead>
                  <tr>
                    <th>Task ID</th>
                    <th>Task Name </th>
                    <th>Date</th>
                    <th>Reporter/Producer Assigned</th>
                    <th>Camera Kit Assigned</th>
                    <th>Driver Assigned</th>
                    <th>DSNG Assigned</th>
                    <th>NLE Assigned</th>
                    <th>Status</th>
                    
                  </tr>
                </thead>
                <tbody aria-relevant="all" aria-live="polite">
                  <tr class="odd">
                    <td>1</td>
                    <td>
                      <h5>PM Event </h5>
                      <small>load ID: 12345678 | Some Dude</small>
                    </td>
                    <td>
                      <div class="text-warning time-from-now">17-Jan-2019</div>
                    </td>
                    <td>
                      <h5>Reporter Name</h5>                      
                    </td>
                    <td>
                      <h5>Camera Kit ID/Name</h5>                     
                    </td>
                    <td>
                      <h5>Driver Name</h5>                     
                    </td>
                    <td>
                      <h5>DSNG 1</h5>                      
                    </td>
                    <td>
                      <h5>NLE 1</h5>
                      
                    </td>
                    <td>Pending</td>
                    
                  </tr>
                  <tr class="even">
                    <!-- <td><i class="icon wb-lock" aria-hidden="true"></i></td> -->
                    <td>2</td>
                    <td>
                      <h5>President Event</h5>                      
                    </td>
                    <td>
                      <div class="text-warning time-from-now">17-Jan-2019</div>
                    </td>
                    <td>
                      <h5>Reporter Name</h5>                      
                    </td>
                    <td>
                      <h5>Camera Kit ID/Name</h5>                      
                    </td>
                    <td>
                      <h5>Driver Name</h5>                      
                    </td>
                    <td>
                      <h5>DSNG 2</h5>                      
                    </td>
                    <td>
                      <h5>NLE 2</h5>                      
                    </td>
                    <td>Completed</td>
                     
                  </tr>
                  
                </tbody>
              </table>
            </div>
          </div>
		</div>
	</div>
</div>
@endsection