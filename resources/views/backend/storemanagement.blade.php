@extends('admin')

  
@section('admincontent')
    <div class="page">
        <!-- Page Content -->
      <div class="page-content">

        <div class="panel">
                <div class="example-wrap">
                  <header class="panel-heading">
                <h3 class="panel-title">
                  STORE EQUIPMENT MANAGEMENT
                </h3>
              </header>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-9">
                <!-- Example Basic Form (Form grid) -->
                  
                  <div class="example">
                    <form action="{{url('test')}}" method="post">
                    {{ csrf_field() }}  
                            
                                    <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Equipment ID:</label>
                                            <div class="col-md-9">
                                              <input type="text" class="form-control" name="Eventname" placeholder="Equipment ID" >
                                            </div>
                                    </div>


                                  <div class="form-group row">
                                      <label class="col-md-3 col-form-label">Equipment Name:</label>
                                        <div class="col-md-9">
                                        <input type="text" class="form-control" name="Cnic" placeholder="Equipment Name" autocomplete="off"
                                        />
                                        </div>
                                  </div>

                                  <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Equipment Type: </label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" name="host" placeholder="Equipment Type" autocomplete="off"/>
                                          </div>
                                    </div>


                                    <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Status of Equipment: </label>
                                          <div class="col-md-9">
                                            <div class="radio-custom radio-default radio-inline">
                                              <input type="radio" id="inputLabelMale" name="inputRadioGender" />
                                              <label for="inputLabelMale">Available</label>
                                            </div>
                                            <div class="radio-custom radio-default radio-inline">
                                              <input type="radio" id="inputLabelFemale" name="inputRadioGender" checked />
                                              <label for="inputLabelFemale">Maintenance</label>
                                            </div>
                                          </div>
                                    </div>  
                                    
                                              <div class="form-group row">
                                                <label class="col-md-3 col-form-label"></label>
                                                <div class="col-md-9">
                                                  <button type="submit" class="btn btn-block btn-primary">Submit </button>
                                                  
                                                </div>
                                              </div>

                      </div>
                    </div>
                  </div> 
            </div>
          </div>
        </div>

        
          </div>
        </div>
      </div>
    </div>
   
    <!-- End Page Content -->
     </div>
@endsection
