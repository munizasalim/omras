@extends('admin')

  
@section('admincontent')
    <div class="page">

<!-- Page -->
      <div class="page-content">

        <!-- Panel Example 1 -->
        <div class="panel">
          <header class="panel-heading">
            <h3 class="panel-title">
              <!-- <a href="" class="btn btn-warning btn-lg">EDIT ASSIGNMENT</a>  -->
            </h3>
          </header>
          <div class="panel-body">
              <a href="{{url('addassignment')}}" class="btn btn-primary btn-lg ">ADD ASSIGNMENT</a>
            <div class="example table-responsive">
              <table class="table table-striped table-bordered" data-plugin="floatThead">
                <thead>
                  <tr>
                    <th>Task ID</th>
                    <th>Task Name </th>
                    <th>Venue</th>
                    <th>Date</th>
                    <th>Time</th>
                    <!-- <th>Reporter/Producer Assigned</th>
                    <th>Camera Kit Assigned</th>
                    <th>Driver Assigned</th>
                    <th>DSNG Assigned</th>
                    <th>NLE Assigned</th> -->
                    <th>Status</th>
                    <th>View</th>
                    <th>Edit/Alter</th>
                    <th>Delete</th>
                    <th>Approval</th>
                  </tr>
                </thead>
                <tbody aria-relevant="all" aria-live="polite">
                  <tr class="odd">
                    <td>1</td>
                    <td>
                      <h5>PM Event </h5>
                      <small>load ID: 12345678 | Some Dude</small>
                    </td>
                    <td>
                      <div class="text-danger time-from-now">Parliament House</div>
                    </td>
                    <td>
                      <div class="text-danger time-from-now">17-Jan-2019</div>
                    </td>
                    <td>
                      <div class="text-danger time-from-now">12PM</div>
                    </td>
                  <!--   <td>
                      <h5>Reporter Name</h5>
                      
                    </td>
                    <td>
                      <h5>Camera Kit ID/Name</h5>
                     
                    </td>
                    <td>
                      <h5>Driver Name</h5>
                     
                    </td>
                    <td>
                      <h5>DSNG 1</h5>
                      
                    </td>
                    <td>
                      <h5>NLE 1</h5>
                      
                    </td> -->
                    <td>Pending</td>
                    <td>
                    <a href="{{url('viewassignment')}}" class="btn btn-warning">View</a>
                    </td>

                    <td>
                    <a href="{{url('editassignment')}}" class="btn btn-info">Edit/Alter</a>
                    </td>

                    <td>
                    <a href="{{url('#')}}" class="btn btn-danger">Delete</a>
                    </td>

                    <td>
                      <input type="checkbox" class="to-labelauty" name="inputLableautyCheckbox" data-plugin="labelauty"
                            data-labelauty="I don't Approve|I Approve (On Air)" />
                    </td>

                  </tr>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- End Panel Example 1 -->

        <!-- Panel Example 2 -->
       
        <!-- End Panel Example 2 -->
      </div>
    </div>

    <!-- End  -->
    </div>
@endsection