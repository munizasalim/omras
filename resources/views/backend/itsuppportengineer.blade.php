@extends('admin')

  
@section('admincontent')

<div class="page">
  <div class="page-content">
    <div class="panel">
        <header class="panel-heading">
            <h3 class="panel-title">
                Frequently Asked Questions (FAQ's)
            </h3>
        </header>
      <div class="row">
          <div class="col-xl-6">
            <!-- Example Default Accordion -->
              <div class="examle-wrap">              
                <div class="example">
                  <div class="panel-group" id="exampleAccordionDefault" aria-multiselectable="true"
                    role="tablist">
                    <div class="panel">
                      <div class="panel-heading" id="exampleHeadingDefaultOne" role="tab">
                        <a class="panel-title" data-toggle="collapse" href="#exampleCollapseDefaultOne"
                          data-parent="#exampleAccordionDefault" aria-expanded="true"
                          aria-controls="exampleCollapseDefaultOne">
                      Collapsible Group Item #1
                    </a>
                      </div>
                      <div class="panel-collapse collapse show" id="exampleCollapseDefaultOne" aria-labelledby="exampleHeadingDefaultOne"
                        role="tabpanel">
                        <div class="panel-body">
                          De moveat laudatur vestra parum doloribus labitur sentire partes, eripuit praesenti
                          congressus ostendit alienae, voluptati ornateque accusamus
                          clamat reperietur convicia albucius, veniat quocirca
                          vivendi aristotele errorem epicurus. Suppetet. Aeternum
                          animadversionis, turbent cn partem porrecta c putamus
                          diceret decore. Vero itaque incursione.
                        </div>
                      </div>
                    </div>
                    <div class="panel">
                      <div class="panel-heading" id="exampleHeadingDefaultTwo" role="tab">
                        <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultTwo"
                          data-parent="#exampleAccordionDefault" aria-expanded="false"
                          aria-controls="exampleCollapseDefaultTwo">
                      Collapsible Group Item #2
                    </a>
                      </div>
                      <div class="panel-collapse collapse" id="exampleCollapseDefaultTwo" aria-labelledby="exampleHeadingDefaultTwo"
                        role="tabpanel">
                        <div class="panel-body">
                          Praestabiliorem. Pellat excruciant legantur ullum leniter vacare foris voluptate
                          loco ignavi, credo videretur multoque choro fatemur mortis
                          animus adoptionem, bello statuat expediunt naturales
                          frequenter terminari nomine, stabilitas privatio initia
                          paranda contineri abhorreant, percipi dixerit incurreret
                          deorsum imitarentur tenetur antiopam latinam haec.
                        </div>
                      </div>
                    </div>
                    <div class="panel">
                      <div class="panel-heading" id="exampleHeadingDefaultThree" role="tab">
                        <a class="panel-title collapsed" data-toggle="collapse" href="#exampleCollapseDefaultThree"
                          data-parent="#exampleAccordionDefault" aria-expanded="false"
                          aria-controls="exampleCollapseDefaultThree">
                      Collapsible Group Item #3
                    </a>
                      </div>
                      <div class="panel-collapse collapse" id="exampleCollapseDefaultThree" aria-labelledby="exampleHeadingDefaultThree"
                        role="tabpanel">
                        <div class="panel-body">
                          Horum, antiquitate perciperet d conspectum locus obruamus animumque perspici probabis
                          suscipere. Desiderat magnum, contenta poena desiderant
                          concederetur menandri damna disputandum corporum equidem
                          cyrenaicisque. Defuturum ultimum ista ignaviamque iudicant
                          feci incursione, reprimique fruenda utamur tu faciam
                          complexiones eo, habeo ortum iucundo artes.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- End Example Default Accordion -->
            </div>
      </div>
      <header class="panel-heading">
          <h3 class="panel-title">
                View Disputes
          </h3>
      </header>
      <div class="panel-body">
            <div class="example table-responsive">
              <table class="table table-striped table-bordered" data-plugin="floatThead">
                <thead>
                  <tr>
                    <th>Dispute ID</th>
                    <th>Title </th>
                    <th>Description</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody aria-relevant="all" aria-live="polite">
                  <tr class="odd">
                    <td>1</td>
                    <td>
                      <h5>PM Event </h5>
                      <small>load ID: 12345678 | Some Dude</small>
                    </td>
                    <td>
                      <h5> description description description description description description </h5>
                    </td>           
                    <td>                      
                      <input type="checkbox" class="to-labelauty" name="inputLableautyCheckbox" data-plugin="labelauty"
                            data-labelauty="Not Resolved |Resolved" />                        
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
      </div>
    </div>
  </div>
</div>
@endsection