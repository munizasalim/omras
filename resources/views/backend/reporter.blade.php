@extends('admin')

  
@section('admincontent')

<div class="page">
  <div class="page-content">
    <div class="panel">
        <header class="panel-heading">
                  <h3 class="panel-title">
                      Assignment
                  </h3>
                </header>
      <div class="panel-body">
            <div class="example table-responsive">
              <table class="table table-striped table-bordered" data-plugin="floatThead">
                <thead>
                  <tr>
                    <th>Task ID</th>
                    <th>Task Name </th>
                    <th>Date Added</th>
                    <th>Reporter/Producer Assigned</th>
                    <th>Camera Kit Assigned</th>
                    <th>Driver Assigned</th>
                    <th>DSNG Assigned</th>
                    <th>NLE Assigned</th>
                    <th>Status</th>
                    <th> Add Content </th>
                    
                  </tr>
                </thead>
                <tbody aria-relevant="all" aria-live="polite">
                  <tr class="odd">
                    <td>1</td>
                    <td>
                      <h5>PM Event </h5>
                      <small>load ID: 12345678 | Some Dude</small>
                    </td>
                    <td>
                      <div class="text-warning time-from-now">17-Jan-2019</div>
                    </td>
                    <td>
                      <h5>Reporter Name</h5>                      
                    </td>
                    <td>
                      <h5>Camera Kit ID/Name</h5>                     
                    </td>
                    <td>
                      <h5>Driver Name</h5>                     
                    </td>
                    <td>
                      <h5>DSNG 1</h5>                      
                    </td>
                    <td>
                      <h5>NLE 1</h5>
                      
                    </td>
                    <td>Pending</td>
                    <td>
                    <a href="{{url('addcontentreporter')}}" class="btn btn-primary">Add Content</a>
                    </td>
                    
                  </tr>
                  
                  
                 
                </tbody>
              </table>
            </div>
          </div>
    </div>
  </div>
</div>
@endsection