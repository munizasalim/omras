@extends('admin')

  
@section('admincontent')
    <div class="page">

<!-- Page -->
      <div class="page-content">

        <!-- Panel Example 1 -->
        <div class="panel">
          <header class="panel-heading">
            <h3 class="panel-title">
              Assignment
            </h3>
          </header>
          <div class="panel-body">
            <div class="example table-responsive">
              <table class="table table-striped table-bordered" data-plugin="floatThead">
                <thead>
                  <tr>
                    <th>Task ID</th>
                    <th>Task Name </th>
                    <th>NLE Assigned</th> 
                    <th>Date</th>
                    <th>Time</th>
                    <th>Status</th>
                    <th>Update Status</th>
                    
                  </tr>
                </thead>
                <tbody aria-relevant="all" aria-live="polite">
                  <tr class="odd">
                    <td>1</td>
                    <td>
                      <h5>PM Event </h5>
                      <small>load ID: 12345678 | Some Dude</small>
                    </td>
                    <td>
                      <h5>NLE 1</h5>
                    </td> 
                    <td>
                      <div class="text-danger time-from-now">17-Jan-2019</div>
                    </td>
                    <td>
                      <div class="text-danger time-from-now">01:30 PM</div>
                    </td>
                    <td>Pending</td>

                    <td>
                    <a href="{{url('UpdateNleStatus')}}" class="btn btn-info">Update Status</a>
                    </td>
                  </tr>
                  <tr class="even">
                    <td>2</td>
                    <td>
                      <h5>President Event </h5>
                      <small>load ID: 12345678 | Some Dude</small>
                    </td>
                    <td>
                      <h5>NLE 1</h5>
                    </td> 
                    <td>
                      <div class="text-danger time-from-now">18-Jan-2019</div>
                    </td>
                    <td>
                      <div class="text-danger time-from-now">02:30 PM</div>
                    </td>
                    <td>Pending</td>

                    <td>
                    <a href="{{url('UpdateNleStatus')}}" class="btn btn-info">Update Status</a>
                    </td>
                  </tr>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- End Panel Example 1 -->

        <!-- Panel Example 2 -->
       
        <!-- End Panel Example 2 -->
      </div>
    </div>

    <!-- End  -->
    </div>
@endsection