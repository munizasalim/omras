@extends('admin')
  
@section('admincontent')

<div class="page">
  <div class="page-content">
    <div class="panel">
      <div class="panel-body">
	      <header class="panel-heading">
            <h3 class="panel-title">
            Assignment
            </h3>
        </header>
        <div class="example table-responsive">
          <table class="table table-striped table-bordered" data-plugin="floatThead">
            <thead>
                <tr>
                  <th>Task ID</th>
                  <th>Task Name </th>
                  <th>Engineer Assigned</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Status</th>                    
                </tr>
            </thead>
            <tbody aria-relevant="all" aria-live="polite">
              <tr class="odd">
                  <td>
                    1
                  </td>
                  <td>
                  <h5>PM Event </h5>
                  <small>load ID: 12345678 | Some Dude</small>
                  </td>
                  <td>
                  <h5>Engineer 1</h5>
                  </td>
                  <td>
                  <div class="text-danger time-from-now">17-Jan-2019</div>
                  </td>
                  <td>
                  <div class="text-danger time-from-now">02:30 PM</div>                     
                  </td>
                  <td>Pending</td>                      
                  </tr>


                  <tr class="even">
                  <td>
                    2
                  </td>
                  <td>
                  <h5>President Event </h5>
                  <small>load ID: 12345678 | Some Dude</small>
                  </td>
                  <td>
                  <h5>Engineer 1</h5>
                  </td>
                  <td>
                  <div class="text-danger time-from-now">18-Jan-2019</div>
                  </td>
                  <td>
                  <div class="text-danger time-from-now">03-30 PM</div>                     
                  </td>
                  
                  <td>Pending</td>                      
                  </tr>


              
            </tbody>
              </table>
        </div>
      </div>
    </div>
</div>
</div>
@endsection