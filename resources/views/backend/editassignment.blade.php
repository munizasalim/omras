@extends('admin')

  
@section('admincontent')
    <div class="page">
        <!-- Page Content -->
      <div class="page-content">

        <div class="panel">
                <div class="example-wrap">
                  <header class="panel-heading">
                <h3 class="panel-title">
                  Assignment Info
                </h3>
              </header>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-9">
                <!-- Example Basic Form (Form grid) -->
                  
                  <div class="example">
                    <form action="{{url('test')}}" method="post">
                    {{ csrf_field() }}  
                    <div class="form-group row">
                                  <label class="col-md-3 col-form-label">Assignment Numer: </label>
                                  <div class="col-md-9">
                              <input type="text" class="form-control" name="Eventname" placeholder="" disabled>
                                  </div>
                            </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Event Name: </label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="Eventname" placeholder="Event Name" autocomplete="off"
                          />
                        </div>
                      </div>

                       <div class="form-group row">
                            <label class="col-md-3 col-form-label"> Venue:</label>
                              <div class="col-md-9">
                                <input type="text" class="form-control" name="venue" placeholder="Venue" autocomplete="off"
                                />
                              </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Time: </label>
                                <div class="input-group col-md-9">
                                  <span class="input-group-addon">
                                    <span class="wb-time"></span>
                                  </span>
                                <input type="text" class="timepicker form-control" name="time" data-plugin="clockpicker" data-autoclose="true">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label class="col-md-3 col-form-label">Date: </label>
                                <div class="input-group col-md-9">
                                    <span class="input-group-addon">
                                      <i class="icon wb-calendar" aria-hidden="true"></i>
                                    </span>
                                    <input type="text" class="form-control" name="date" data-plugin="datepicker" data-multidate="true">
                                  </div>
                             </div>

                              <div class="form-group row">
                              <label class="col-md-3 col-form-label">Host:</label>
                                <div class="col-md-9">
                                  <input type="text" class="form-control" name="host" placeholder="Host" autocomplete="off"
                                  />
                                </div>
                              </div>

                              <div class="form-group row">
                              <label class="col-md-3 col-form-label">Host Contact No: </label>
                                <div class="col-md-9">
                                  <input type="text" class="form-control" name="hostcontact" placeholder="Host Contact No" autocomplete="off"
                                  />
                                </div>
                              </div>
                       
                              <div class="form-group row">
                                  <label class="col-md-3 col-form-label">Description: </label>
                                  <div class="col-md-9">
                                      <textarea class="form-control" placeholder="Add Description" name="description" rows="3">
                                      </textarea>
                                  </div>
                            </div>                                                                        
                      </div>
                    </div>
                  </div> 
            </div>
          </div>
        </div>

        <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">
                  ADD HR
                </h3>
              </header>
              <div class="panel-body container-fluid">

                <div class="row row-lg">

                  <div class="col-md-12 col-lg-6" >
                    <!-- Example Horizontal Form -->
                    <div class="example-wrap">
                     
                     <!--  <h4 class="example-title">Add Assignment Form</h4> -->
                      <p>
                       
                      </p>
                      <div class="example">
                       
                         

                           <div class="form-group row">
                              <label class="col-md-3 col-form-label">Assign Reporter/Producer</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <select class="form-control" multiple data-plugin="select2" name="reporter[]">
                                            <optgroup label="">
                                              
                                              <option value="AK">Reporter 1 </option>
                                              <option value="HI">Reporter 2</option>
                                              <option value="HI">Reporter 3</option>
                                              <option value="HI">Reporter 4</option>
                                              <option value="HI">Reporter 5</option>
                                              <option value="HI">Reporter 6</option>
                                            </optgroup>
                                            
                                           
                                          </select>
                                      </div>
                                    </div> 
                                  </div>

                                      <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Assign Camera Man:</label>
                                        <div class="col-md-9">
                                              <div class="example">
                                                <select class="form-control" multiple data-plugin="select2" name="cameraman[]">
                                                      <option value="qq">Camera Man 1</option>
                                                      <option value="ww">Camera Man 2</option>
                                                      <option value="ee">Camera Man 3</option>
                                                      <option value="rr">Camera Man 4</option>
                                                      <option value="tt">Camera Man 5</option>
                                                    </select>
                                                </div>
                                              </div> 
                                            </div>

                                       <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Assign Camera:</label>
                                        <div class="col-md-9">
                                              <div class="example">
                                                    <select class="form-control" multiple data-plugin="select2" name="camera[]">
                                                      <option value="ca">Camera 1</option>
                                                      <option value="acm">Camera 2</option>
                                                      <option value="qwe">Camera 3</option>
                                                      <option value="ew">Camera 4</option>
                                                      <option value="weq">Camera 5</option>
                                                    </select>
                                                </div>
                                              </div> 
                                            </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Comments: </label>
                        <div class="col-md-9">
                          <textarea class="form-control" placeholder="Briefly Describe Yourself" name="comment"></textarea>
                        </div>
                      </div>

                      <div class="form-group row">
                        <label class="col-md-3 col-form-label"></label>
                        <div class="col-md-9">
                          <button type="submit" class="btn btn-block btn-primary">Generate Duty Roster </button>
                          <a href="{{url('assignment')}}" class="btn btn-block btn-danger">Back </a>
                        </div>
                      </div>
                     
                  </div>
                </div>
                
              </div>

              <div class="col-md-12 col-lg-6">
               
                <div class="example-wrap">
                 
                  <div class="example">
                    
                      <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Assign DSNG:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <select class="form-control" data-plugin="select2" name="dsng">
                                                      <optgroup label="">
                                                        <option value="" disabled selected hidden>Select DSNG</option>
                                                        <option value="AK">DSNG 1 </option>
                                                        <option value="HI">DSNG 2</option>
                                                        <option value="HI">DSNG 3</option>
                                                        <option value="HI">DSNG 4</option>
                                                        <option value="HI">DSNG 5</option>
                                                        <option value="HI">DSNG 6</option>
                                                      </optgroup>
                                                    </select>
                                                </div>
                                              </div> 
                                            </div>

                                              <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Assign Vehicle:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <select class="form-control" multiple data-plugin="select2" name="vehicle[]">
                                                      <optgroup label="">
                                                        <option value="AK">Vehicle 1 </option>
                                                        <option value="HI">Vehicle 2</option>
                                                        <option value="HI">Vehicle 3</option>
                                                        <option value="HI">Vehicle 4</option>
                                                        <option value="HI">Vehicle 5</option>
                                                        <option value="HI">Vehicle 6</option>
                                                      </optgroup>
                                                    </select>
                                                </div>
                                              </div> 
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Add NLE:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                   <select class="form-control" multiple data-plugin="select2" name="nle">
                                                        <optgroup label="ADD NLE">
                                                          <option value="AK">NLE 1</option>
                                                          <option value="HI">NLE 2</option>
                                                          <option value="HI">NLE 3</option>
                                                          <option value="HI">NLE 4</option>
                                                          <option value="HI">NLE 5</option>

                                                        </optgroup>
                                                    </select>
                                                </div>
                                              </div> 
                                            </div>
                                    
                    </form>
                  </div>
                </div>
               
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
   
    <!-- End Page Content -->
     </div>
@endsection
