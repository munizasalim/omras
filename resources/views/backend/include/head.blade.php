
    <title> NOMRAS | Admin Dashboard</title>
   
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
   <html lang="en" dir="ltr">
	<!-- <link rel="apple-touch-icon" href="../../assets/images/apple-touch-icon.png"> -->
    <link rel="shortcut icon" href="../../assets/images/favicon.ico">
    
    <!-- Stylesheets -->
     <link rel="stylesheet" href="{{asset('/admin/assets/global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/base/assets/css/site.min.css')}}">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/flag-icon-css/flag-icon.css')}}">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/material-design/material-design.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/font-awesome/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/web-icons/web-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    

    <!-- SELECT PLUGINS -->

    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/select2/select2.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/bootstrap-select/bootstrap-select.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/icheck/icheck.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/switchery/switchery.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/asrange/asRange.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/ionrangeslider/ionrangeslider.min.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/asspinner/asSpinner.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/clockpicker/clockpicker.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/ascolorpicker/asColorPicker.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/jquery-labelauty/jquery-labelauty.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/timepicker/jquery-timepicker.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/jquery-strength/jquery-strength.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/multi-select/multi-select.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/typeahead-js/typeahead.css')}}">
        <link rel="stylesheet" href="{{asset('/admin/assets/base/assets/examples/css/forms/advanced.css')}}">