<div class="site-menubar-footer ">
        <a href="javascript: void(0);" class="fold-show" data-placement="top" data-toggle="tooltip"
          data-original-title="Settings">
          <span class="icon wb-settings" aria-hidden="true"></span>
        </a>
        <a href="javascript: void(0);" data-placement="top" data-toggle="tooltip" data-original-title="Lock">
          <span class="icon wb-eye-close" aria-hidden="true"></span>
        </a>
        <a href="{{ url('logout') }}" data-placement="top" data-toggle="tooltip" data-original-title="Logout">
          <span class="icon wb-power" aria-hidden="true"></span>
        </a>
      </div></div>    
      <div class="site-gridmenu">
      <div>
        <div>
          <ul>
            <li>
              <a href="../apps/mailbox/mailbox.html">
                <i class="icon wb-envelope"></i>
                <span>Mailbox</span>
              </a>
            </li>
            <li>
              <a href="../apps/calendar/calendar.html">
                <i class="icon wb-calendar"></i>
                <span>Calendar</span>
              </a>
            </li>
            <li>
              <a href="../apps/contacts/contacts.html">
                <i class="icon wb-user"></i>
                <span>Contacts</span>
              </a>
            </li>
            <li>
              <a href="../apps/media/overview.html">
                <i class="icon wb-camera"></i>
                <span>Media</span>
              </a>
            </li>
            <li>
              <a href="../apps/documents/categories.html">
                <i class="icon wb-order"></i>
                <span>Documents</span>
              </a>
            </li>
            <li>
              <a href="../apps/projects/projects.html">
                <i class="icon wb-image"></i>
                <span>Project</span>
              </a>
            </li>
            <li>
              <a href="../apps/forum/forum.html">
                <i class="icon wb-chat-group"></i>
                <span>Forum</span>
              </a>
            </li>
            <li>
              <a href="../index.html">
                <i class="icon wb-dashboard"></i>
                <span>Dashboard</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
 <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">Copyrights © Pakistan Televison Corp. Ltd. All rights reserved.<a href="#"></a></div>
     <!--  <div class="site-footer-right">
        Crafted with <i class="red-600 wb wb-heart"></i> by <a href="https://themeforest.net/user/creation-studio">Creation Studio</a>
      </div> -->
    </footer>

    <!-- Core  -->
    <script src="{{asset('/admin/assets/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/jquery/jquery.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/popper-js/umd/popper.min.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/bootstrap/bootstrap.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/animsition/animsition.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
    
    <!-- Plugins -->
    <script src="{{asset('/admin/assets/global/vendor/switchery/switchery.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/intro-js/intro.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/screenfull/screenfull.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>

        <script src="{{asset('/admin/assets/global/vendor/select2/select2.full.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/bootstrap-select/bootstrap-select.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/icheck/icheck.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/switchery/switchery.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/asrange/jquery-asRange.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/ionrangeslider/ion.rangeSlider.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/asspinner/jquery-asSpinner.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/clockpicker/bootstrap-clockpicker.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/ascolor/jquery-asColor.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/asgradient/jquery-asGradient.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/ascolorpicker/jquery-asColorPicker.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/bootstrap-maxlength/bootstrap-maxlength.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/jquery-knob/jquery.knob.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/jquery-labelauty/jquery-labelauty.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/timepicker/jquery.timepicker.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/datepair/datepair.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/datepair/jquery.datepair.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/jquery-strength/password_strength.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/jquery-strength/jquery-strength.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/multi-select/jquery.multi-select.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/typeahead-js/bloodhound.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/typeahead-js/typeahead.jquery.min.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/switchery/switchery.js')}}"></script>
    
    <!-- Scripts -->
    <script src="{{asset('/admin/assets/global/js/Component.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Base.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Config.js')}}"></script>
    
    <script src="{{asset('/admin/assets/base/assets/js/Section/Menubar.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/GridMenu.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/Sidebar.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/PageAside.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Plugin/menu.js')}}"></script>
    
    <script src="{{asset('/admin/assets/global/js/config/colors.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/config/tour.js')}}"></script>
    <script>Config.set("{{asset('assets', '/admin/assets/base/assets')}}");</script>
    
    <!-- Page -->
    <script src="{{asset('/admin/assets/base/assets/js/Site.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/asscrollable.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/slidepanel.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/switchery.js')}}"></script>

          <script src="{{asset('/admin/assets/global/js/Plugin/select2.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/bootstrap-tokenfield.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/bootstrap-tagsinput.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/bootstrap-select.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/icheck.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/switchery.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/asrange.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/ionrangeslider.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/asspinner.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/clockpicker.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/ascolorpicker.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/bootstrap-maxlength.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/jquery-knob.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/bootstrap-touchspin.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/card.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/jquery-labelauty.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/bootstrap-datepicker.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/jt-timepicker.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/datepair.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/jquery-strength.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/multi-select.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/jquery-placeholder.js')}}"></script>
    
        <script src="{{asset('/admin/assets/base/assets/examples/js/forms/advanced.js')}}"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>

<script src="{{asset('/admin/assets/global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
      Breakpoints();
    </script>