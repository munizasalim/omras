
   
    <div class="site-menubar">
      <div class="site-menubar-body">
        <div>
          <div>
            <ul class="site-menu" data-plugin="menu">
              <li class="site-menu-category">General</li>
              @if(Auth::check() && Auth::user()->name == 'Controller News' || Auth::user()->name == 'admin')

              <li class="site-menu-item has-sub">
                <a href="{{url('dashboard')}}">
                        <i class="site-menu-icon wb-dashboard" aria-hidden="true"></i>
                        <span class="site-menu-title">Dashboard</span>
                            <!-- <div class="site-menu-badge">
                                <span class="badge badge-pill badge-success"></span>
                            </div> -->
                    </a>
                
              </li>
              @endif
              <!-- <li class="site-menu-item has-sub">
                <a href="{{url('addassignment')}}">
                        <i class="site-menu-icon wb-file" aria-hidden="true"></i>
                        <span class="site-menu-title">Add Assignment </span>
                                <span class="site-menu-arrow"></span>
                    </a>
                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="../layouts/menu-collapsed.html">
                      <span class="site-menu-title">Menu Collapsed</span>
                    </a>
                  </li> 
                  </li>
                </ul>
              </li> -->

            
               @if(Auth::check() && Auth::user()->name == 'Controller News' || Auth::user()->name == 'admin')
              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-library" aria-hidden="true"></i>
                        <span class="site-menu-title">CR/DCR</span>
                                <span class="site-menu-arrow"></span>
                    </a>
                
                <ul class="site-menu-sub">
                  <li class="site-menu-item">
                    <a class="animsition-link" href="{{url('assignment')}}">
                      <span class="site-menu-title">Assignment</span>
                    </a>
                  </li>
                  <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
                  
                  
                  </li>
                </ul>
              </li>
              @endif

              @if(Auth::check() && Auth::user()->name == 'Reporter1' || Auth::user()->name == 'admin')
                <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-file" aria-hidden="true"></i>
                        <span class="site-menu-title">Reporter/Producer </span>
                          <span class="site-menu-arrow"></span>     
                    </a> 
                    <ul class="site-menu-sub">
                       <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reporter')}}">
                          <span class="site-menu-title">Assignment</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
 
                  </li>
                </ul>
              </li>
              @endif

               @if(Auth::check() && Auth::user()->name == 'chief camera man' || Auth::user()->name == 'admin') 
              <li class="site-menu-item has-sub ">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-library" aria-hidden="true"></i>
                            <span class="site-menu-title">Chief Cameraman </span>
                                <span class="site-menu-arrow"></span>
                    </a> 

                    <ul class="site-menu-sub">

                      <li class="site-menu-item">
                        <a class="animsition-link" href="">
                          <span class="site-menu-title">Assignment</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('addhr')}}">
                          <span class="site-menu-title">Add HR</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
                  
                  
                  </li>
                </ul>
              </li>
                @endif

               @if(Auth::check() && Auth::user()->name == 'camera man' || Auth::user()->name == 'admin')
              <li class="site-menu-item has-sub">

    
                <a href="{{url('cameraman')}}">
                        <i class="site-menu-icon wb-library" aria-hidden="true"></i>

                        <span class="site-menu-title">Cameraman </span>
                                <!-- <span class="site-menu-arrow"></span> -->
                    </a> 
              </li>
                @endif

          @if(Auth::check() && Auth::user()->name == 'Shift Incharge Engineering' || Auth::user()->name == 'admin')      
          <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-table" aria-hidden="true"></i>
                        <span class="site-menu-title">Shift Incharge(Engg.) </span>
                         <span class="site-menu-arrow"></span>      
                    </a> 
                    <ul class="site-menu-sub">

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('chiefengineer')}}">
                          <span class="site-menu-title">Assignment</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('addhr')}}">
                          <span class="site-menu-title">Add/Update HR</span>
                        </a>
                      </li>
                       <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('maintenanceofequipment')}}">
                          <span class="site-menu-title">Equipment Management</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
                  
                  
                  </li>
                </ul>
              </li>
          @endif

            @if(Auth::check() && Auth::user()->name == 'Engineer' || Auth::user()->name == 'admin')     
               <li class="site-menu-item has-sub">
                <a href="{{url('engineer')}}">
                        <i class="site-menu-icon wb-table" aria-hidden="true"></i>
                        <span class="site-menu-title">Engineer </span>
                               
                    </a> 
              </li>
             @endif 

             @if(Auth::check() && Auth::user()->name == 'Shift Incharge NLE' || Auth::user()->name == 'admin')     
              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-hammer" aria-hidden="true"></i>
                        <span class="site-menu-title">Shift Incharge NLE  </span>
                          <span class="site-menu-arrow"></span>     
                    </a> 
                    <ul class="site-menu-sub">

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('shiftinchargenle')}}">
                          <span class="site-menu-title">Assignment</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('addhr')}}">
                          <span class="site-menu-title">Add/Update HR</span>
                        </a>
                      </li>
                       <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('NleMachineMaintenance')}}">
                          <span class="site-menu-title">Equipment Management</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
                  
                  
                  </li>
                </ul>
              </li>
              @endif

              @if(Auth::check() && Auth::user()->name == 'Non linear Editor' || Auth::user()->name == 'admin')     
              <li class="site-menu-item has-sub">
                <a href="{{url('nleview')}}">
                        <i class="site-menu-icon wb-hammer" aria-hidden="true"></i>
                        <span class="site-menu-title">NLE </span>
                               
                    </a> 
              </li>
              @endif

              @if(Auth::check() && Auth::user()->name == 'Shift Incharge Store' || Auth::user()->name == 'admin')     
               <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-library" aria-hidden="true"></i>
                        <span class="site-menu-title">Shift Incharge Store </span>
                        <span class="site-menu-arrow"></span> 
                               
                    </a>
                    <ul class="site-menu-sub">

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('shiftinchargestore')}}">
                          <span class="site-menu-title">Assignment</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('addhr')}}">
                          <span class="site-menu-title">Add/Update HR</span>
                        </a>
                      </li>
                       <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('storemanagement')}}">
                          <span class="site-menu-title">Assets Maintenance/Management</span>
                        </a>
                      </li>

                      
                  
                  
                  </li>
                </ul> 
              </li>
              @endif

              @if(Auth::check() && Auth::user()->name == 'Store Keeper' || Auth::user()->name == 'admin') 
              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-library" aria-hidden="true"></i>
                        <span class="site-menu-title">Store Keeper </span>
                        <span class="site-menu-arrow"></span> 
                               
                    </a> 
                    <ul class="site-menu-sub">

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('storekeeper')}}">
                          <span class="site-menu-title">Assignment</span>
                        </a>
                      </li>

                       <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('storemanagement')}}">
                          <span class="site-menu-title">Assets Maintenance/Management</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
                      
                  
                  
                  </li>
                </ul> 
              </li>
              @endif

              @if(Auth::check() && Auth::user()->name == 'Transport Manager' || Auth::user()->name == 'admin') 
              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                        <span class="site-menu-title">Transport Manager </span>
                        <span class="site-menu-arrow"></span>        
                    </a> 
                    <ul class="site-menu-sub">

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('transportmanager')}}">
                          <span class="site-menu-title">Assignment</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('addhr')}}">
                          <span class="site-menu-title">Add/Update HR</span>
                        </a>
                      </li>

                       <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('vehiclemanagement')}}">
                          <span class="site-menu-title">Maintenance of Vehicle</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
 
                  </li>
                </ul> 
              </li>
              @endif

              @if(Auth::check() && Auth::user()->name == 'Driver' || Auth::user()->name == 'admin') 
              <li class="site-menu-item has-sub">
                <a href="{{url('driver')}}">
                        <i class="site-menu-icon wb-grid-4" aria-hidden="true"></i>
                        <span class="site-menu-title">Driver </span>
                               
                    </a> 
              </li>

              @endif

              @if(Auth::check() && Auth::user()->name == 'IT Support Engineer' || Auth::user()->name == 'admin') 
              <li class="site-menu-item has-sub">
                <a href="javascript:void(0)">
                        <i class="site-menu-icon wb-hammer" aria-hidden="true"></i>
                        <span class="site-menu-title">IT Support Engineer </span>
                        <span class="site-menu-arrow"></span>
                               
                    </a> 
                      <ul class="site-menu-sub">
                       <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('itsupport')}}">
                          <span class="site-menu-title">FAQs</span>
                        </a>
                      </li>

                      <li class="site-menu-item">
                        <a class="animsition-link" href="{{url('reportdispute')}}">
                          <span class="site-menu-title">Report Dispute</span>
                        </a>
                      </li>
 
                  </li>
                </ul>
              </li>
              @endif
            
                  </div>
        </div>
      </div>
    </div>
  