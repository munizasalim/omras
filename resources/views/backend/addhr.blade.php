@extends('admin')

  
@section('admincontent')
    <div class="page">
        <!-- Page Content -->
      <div class="page-content">

        <div class="panel">
                <div class="example-wrap">
                  <header class="panel-heading">
                <h3 class="panel-title">
                  ADD HUMAN RESOURCE 
                </h3>
              </header>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-9">
                <!-- Example Basic Form (Form grid) -->
                  
                  <div class="example">
                    <form action="{{url('test')}}" method="post">
                    {{ csrf_field() }}  
                            
                                    <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Name:</label>
                                            <div class="col-md-9">
                                              <input type="text" class="form-control" name="Eventname" placeholder="Enter Name" >
                                            </div>
                                    </div>


                                  <div class="form-group row">
                                      <label class="col-md-3 col-form-label">CNIC: </label>
                                        <div class="col-md-9">
                                        <input type="number" class="form-control" name="Cnic" placeholder="Enter CNIC" autocomplete="off"
                                        />
                                        </div>
                                  </div>

                                  <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Personnel Number: </label>
                                          <div class="col-md-9">
                                            <input type="number" class="form-control" name="host" placeholder="Enter Employee Personnel Number" autocomplete="off"/>
                                          </div>
                                    </div>


                                   <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Mobile Number:</label>
                                          <div class="col-md-9">
                                            <input type="Number" class="form-control" name="MobileNumber" placeholder="Enter Mobile Number" autocomplete="off"/>
                                          </div>
                                        </div>

                                  <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Address:</label>
                                          <div class="col-md-9">
                                            <input type="text" class="form-control" name="Address" placeholder="Enter Address " autocomplete="off"/>
                                          </div>
                                        </div>


                                   <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Designation:</label>
                                          <div class="col-md-9">
                                        <input type="text" class="form-control" name="Designation" placeholder="Enter Designation" autocomplete="off"/>
                                          </div>
                                        </div>

                                      <div class="form-group row">
                                      <label class="col-md-3 col-form-label">Station:</label>
                                        <div class="col-md-9">
                                          <input type="text" class="form-control" name="host" placeholder="Enter Duty Station" autocomplete="off"/>
                                        </div>
                                      </div>

                                      <div class="form-group row">
                                      <label class="col-md-3 col-form-label">Upload Picture: </label>
                                        <div class="col-md-9">
                                          <div class="input-group input-group-file" data-plugin="inputGroupFile">
                                            <input type="text" class="form-control" readonly="">
                                            <span class="input-group-btn">
                                              <span class="btn btn-success btn-file">
                                                <i class="icon wb-upload" aria-hidden="true"></i>
                                                <input type="file" name="" multiple="">
                                              </span>
                                            </span>
                                          </div>
                                        </div>
                                      </div>
                       
                                      

                                    <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Status of Availability: </label>
                                          <div class="col-md-9">
                                            <div class="radio-custom radio-default radio-inline">
                                              <input type="radio" id="inputLabelMale" name="inputRadioGender" />
                                              <label for="inputLabelMale">Available</label>
                                            </div>
                                            <div class="radio-custom radio-default radio-inline">
                                              <input type="radio" id="inputLabelFemale" name="inputRadioGender" checked />
                                              <label for="inputLabelFemale">Not Available</label>
                                            </div>
                                          </div>
                                    </div>  
                                    <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Shift Time Start: </label>
                                          <div class="input-group col-md-9">
                                              <span class="input-group-addon">
                                                <span class="wb-time"></span>
                                              </span>
                                            <input type="text" class="timepicker form-control" name="time" data-plugin="clockpicker" data-autoclose="true">
                                            </div>
                                    </div>  

                                    <div class="form-group row">
                                          <label class="col-md-3 col-form-label">Shift Time End: </label>
                                          <div class="input-group col-md-9">
                                              <span class="input-group-addon">
                                                <span class="wb-time"></span>
                                              </span>
                                            <input type="text" class="timepicker form-control" name="time" data-plugin="clockpicker" data-autoclose="true">
                                            </div>
                                    </div>  
                                              <div class="form-group row">
                                                <label class="col-md-3 col-form-label"></label>
                                                <div class="col-md-9">
                                                  <button type="submit" class="btn btn-block btn-primary">Submit </button>
                                                  
                                                </div>
                                              </div>

                      </div>
                    </div>
                  </div> 
            </div>
          </div>
        </div>

        
          </div>
        </div>
      </div>
    </div>
   
    <!-- End Page Content -->
     </div>
@endsection
