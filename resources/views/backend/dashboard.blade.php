@extends('admin')

  
@section('admincontent')
<!-- Page -->
    <div class="page">
      <div class="page-header">
        <h1 class="page-title font-size-26 font-weight-100">Assignments Overview</h1>
      </div>

      <div class="page-content container-fluid">
        <div class="row">
          <!-- First Row -->
          <div class="col-xl-3 col-md-6 info-panel">
            <div class="card card-shadow">
              <div class="card-block bg-white p-20">
                <button type="button" class="btn btn-floating btn-sm btn-warning">
                  <i class="icon wb-shopping-cart"></i>
                </button>
                <span class="ml-15 font-weight-400">Today Events</span>
                <div class="content-text text-center mb-0">
                  <!-- <i class="text-danger icon wb-triangle-up font-size-20">
              </i> -->
                  <span class="font-size-40 font-weight-100">5</span>
                  <!-- <p class="blue-grey-400 font-weight-100 m-0">+45% From previous month</p> -->
                </div>
              </div>
            </div>
          </div>
          <div class="col-xl-3 col-md-6 info-panel">
            <div class="card card-shadow">
              <div class="card-block bg-white p-20">
                <button type="button" class="btn btn-floating btn-sm btn-danger">
                  <i class="icon fa-dollar"></i>
                </button>
                <span class="ml-15 font-weight-400">Total Events Covered</span>
                <div class="content-text text-center mb-0">
                  <!-- <i class="text-success icon wb-triangle-down font-size-20">
              </i> -->
                  <span class="font-size-40 font-weight-100">10</span>
                  <!-- <p class="blue-grey-400 font-weight-100 m-0">+45% From previous month</p> -->
                </div>
              </div>
            </div>
          </div>
          <!-- <div class="col-xl-3 col-md-6 info-panel">
            <div class="card card-shadow">
              <div class="card-block bg-white p-20">
                <button type="button" class="btn btn-floating btn-sm btn-success">
                  <i class="icon wb-eye"></i>
                </button>
                <span class="ml-15 font-weight-400"></span>
                <div class="content-text text-center mb-0">
                  <i class="text-danger icon wb-triangle-up font-size-20">
              </i>
                  <span class="font-size-40 font-weight-100"></span>
                  <p class="blue-grey-400 font-weight-100 m-0"></p>
                </div>
              </div>
            </div>
          </div> -->
          <!-- <div class="col-xl-3 col-md-6 info-panel">
            <div class="card card-shadow">
              <div class="card-block bg-white p-20">
                <button type="button" class="btn btn-floating btn-sm btn-primary">
                  <i class="icon wb-user"></i>
                </button>
                <span class="ml-15 font-weight-400"></span>
                <div class="content-text text-center mb-0">
                  <i class="text-danger icon wb-triangle-up font-size-20">
              </i>
                  <span class="font-size-40 font-weight-100"></span>
                  <p class="blue-grey-400 font-weight-100 m-0"></p>
                </div>
              </div>
            </div>
          </div> -->
          <!-- End First Row -->

          <!-- second Row -->
          
                  
          <div class="col-lg-12" id="ecommerceRecentOrder">
            <div class="card card-shadow table-row">
              <div class="card-header card-header-transparent py-20">
                <div class="btn-group dropdown">
                  <h3>Recent Assignment</h3>
                  <!-- <div class="dropdown-menu animate" role="menu">
                    <a class="dropdown-item" href="#" role="menuitem">Sales</a>
                    <a class="dropdown-item" href="#" role="menuitem">Total sales</a>
                    <a class="dropdown-item" href="#" role="menuitem">profit</a>
                  </div> -->
                </div>
              </div>
              <div class="card-block bg-white table-responsive">
                <table class="table">
                  <thead>
                    <tr>
                      
                      <th>Event Name</th>
                      <th>Assigned To</th>
                      <th>Date Assigned </th>
                      <th>Status</th>
                      
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      
                      <td>PM Event </td>
                      <td>Reporter 1</td>
                      <td>22/08/2019</td>
                      <td>
                        <span class="badge badge-success font-weight-100">Complete</span>
                      </td>
                      
                    </tr>
                    <tr>
                      
                      <td>President Event</td>
                      <td>Reporter 2</td>
                      <td>15/07/2019</td>
                      <td>
                        <span class="badge badge-warning font-weight-100">Pending</span>
                      </td>
                      
                    </tr>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <!-- End Third Left -->

          <!-- Third Right -->
    <!--       <div class="col-lg-4" id="ecommerceRevenue">
            <div class="card card-shadow text-center pt-10">
              <h3 class="card-header card-header-transparent blue-grey-700 font-size-14 mt-0">REVENUE</h3>
              <div class="card-block bg-white">
                <div class="ct-chart barChart"></div>
                <div class="pie-view row">
                  <div class="col-6 pie-left text-center">
                    <h5 class="blue-grey-500 font-size-14 font-weight-100">GROS REVENUE</h5>
                    <p class="font-size-20 blue-grey-700">
                      9,362,74
                    </p>
                    <div class="pie-progress pie-progress-sm" data-plugin="pieProgress" data-valuemax="100"
                      data-valuemin="0" data-barcolor="#a57afa" data-size="100" data-barsize="4"
                      data-goal="60" aria-valuenow="60" role="progressbar">
                      <span class="pie-progress-number">60%</span>
                    </div>
                  </div>
                  <div class="col-6 pie-right text-center">
                    <h5 class="blue-grey-500 font-size-14 font-weight-100">NET REVENUE</h5>
                    <p class="font-size-20 blue-grey-700">
                      6,734,58
                    </p>
                    <div class="pie-progress pie-progress-sm" data-plugin="pieProgress" data-valuemax="100"
                      data-valuemin="0" data-barcolor="#28c0de" data-size="100" data-barsize="4"
                      data-goal="78" aria-valuenow="78" role="progressbar">
                      <span class="pie-progress-number">78%</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div> -->
          <!-- End Third Right -->
          <!-- End Third Row -->
        </div>
      </div>
    </div>
      

    </div>
    <!-- End Page -->
@endsection
