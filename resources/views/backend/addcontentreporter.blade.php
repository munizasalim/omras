@extends('admin')

  
@section('admincontent')
    <div class="page">
        <!-- Page Content -->
      <div class="page-content">

        <div class="panel">
                <div class="example-wrap">
                  <header class="panel-heading">
                <h3 class="panel-title">
                 Add Content to Assignment 
                </h3>
              </header>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-9">
                <!-- Example Basic Form (Form grid) -->
                  
                  <div class="example">
                    <form action="{{url('test')}}" method="post">
                    {{ csrf_field() }}  
                    <div class="form-group row">
                                  <label class="col-md-3 col-form-label">Assignment Numer: </label>
                                  <div class="col-md-9">
                                    <input type="text" class="form-control" value="1" name="Assignment Numer" placeholder="" disabled>
                                  </div>
                            </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Event Name: </label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="Eventname" placeholder="Event Name" disabled autocomplete="off"
                          />
                        </div>
                      </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Story No: </label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="Eventname" value="1" disabled autocomplete="off"
                          />
                        </div>
                      </div>
                             <div class="form-group row">
                                  <label class="col-md-3 col-form-label">Slug: </label>
                                  <div class="col-md-9">
                                      <textarea class="form-control" placeholder="Add Description" name="slug" rows="1">
                                      </textarea>
                                  </div>
                            </div> 

                              <div class="form-group row">
                                  <label class="col-md-3 col-form-label">Story: </label>
                                  <div class="col-md-9">
                                      <textarea class="form-control" placeholder="Add Description" name="story" rows="8">
                                      </textarea>
                                  </div>
                            </div> 

                            <div class="form-group row">
                              <label class="col-md-3 col-form-label">Talking Heads: </label>
                              <div class="col-md-9">
                                <input type="text" class="form-control" name="Eventname"   autocomplete="off"
                                />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-md-3 col-form-label">Voice Over: </label>
                              <div class="col-md-9">
                                <input type="text" class="form-control" name="Eventname"   autocomplete="off"
                                />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-md-3 col-form-label">On Camera: </label>
                              <div class="col-md-9">
                                <input type="text" class="form-control" name="Eventname"   autocomplete="off"
                                />
                              </div>
                            </div>

                            <div class="form-group row">
                              <label class="col-md-3 col-form-label">Political Party: </label>
                              <div class="col-md-9">
                                <input type="text" class="form-control" name="Eventname"   autocomplete="off"
                                />
                              </div>
                            </div>   

                            <div class="form-group row">
                                <label class="col-md-3 col-form-label">Time: </label>
                                <div class="input-group col-md-9">
                                  <span class="input-group-addon">
                                    <span class="wb-time"></span>
                                  </span>
                                <input type="text" class="timepicker form-control" name="time" data-plugin="clockpicker" data-autoclose="true">
                                </div>
                            </div>


                              <div class="form-group row">
                                <label class="col-md-3 col-form-label">Date: </label>
                                <div class="input-group col-md-9">
                                    <span class="input-group-addon">
                                      <i class="icon wb-calendar" aria-hidden="true"></i>
                                    </span>
                                    <input type="text" class="form-control" name="date" data-plugin="datepicker" data-multidate="true">
                                  </div>
                             </div>
                       
                             <div class="form-group row">
                                  <label class="col-md-3 col-form-label"></label>
                                  <div class="col-md-9">
                                  <button type="submit" class="btn btn-block btn-primary">Submit </button>
                                  <a href="{{url('reporter')}}" class="btn btn-block btn-danger">Back </a>
                              </div>
                            </div> 
                                                                                                        
                      </div>
                    </div>
                  </div> 
            </div>
          </div>
        </div>

        
            
        
   
    <!-- End Page Content -->
     </div>
@endsection
