@extends('admin')

  
@section('admincontent')
    <div class="page">
        <!-- Page Content -->
      <div class="page-content">

        <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">
                  Assignment Info
                </h3>
              </header>
              <div class="panel-body container-fluid">

                <div class="row row-lg">

                  <div class="col-md-12 col-lg-6" >      
                    <div class="example-wrap">
                      <div class="example">
                           <div class="form-group row">
                              <label class="col-md-3 col-form-label">Assignment Numer:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="Eventname" placeholder="" disabled>
                                      </div>
                                    </div> 
                                  </div>

                                      <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Event Name:</label>
                                        <div class="col-md-9">
                                              <div class="example">
                                               <input type="text" class="form-control" name="Eventname" placeholder="Event Name" autocomplete="off" disabled />
                                                </div>
                                              </div> 
                                            </div>

                                       <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Venue:</label>
                                        <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="venue" placeholder="Venue" autocomplete="off" disabled/>
                                                </div>
                                              </div> 
                                            </div>  

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Time:</label>
                                                <div class="col-md-9">
                                                  <div class="example">
                                                     <input type="text" class="timepicker form-control" name="time" data-plugin="clockpicker" data-autoclose="true" disabled >
                                                </div>
                                              </div> 
                                            </div>

                                           



                                  </div>
                              </div>      
                        </div>


                          <div class="col-md-12 col-lg-6">
                            <div class="example-wrap">
                              <div class="example">
                                          

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Date:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" data-plugin="datepicker" data-multidate="true" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Host:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                             <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Host Contact No:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>


                                             <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Discription</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <textarea class="form-control" placeholder="Briefly Describe Yourself" name="comment" disabled></textarea>
                                                </div>
                                              </div> 
                                            </div> 
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
      
  


        <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">
                  Human Resource Assigned 
                </h3>
              </header>
              <div class="panel-body container-fluid">

                <div class="row row-lg">

                  <div class="col-md-12 col-lg-6" >      
                    <div class="example-wrap">
                      <div class="example">
                           <div class="form-group row">
                              <label class="col-md-3 col-form-label">Assigned Reporter/Producer</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="date" disabled>
                                      </div>
                                    </div> 
                                  </div>

                                      <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Assigned Camera Man:</label>
                                        <div class="col-md-9">
                                              <div class="example">
                                                <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div> 

                                            <div class="form-group row">
                                        <label class="col-md-3 col-form-label">Assigned Camera:</label>
                                        <div class="col-md-9">
                                              <div class="example">
                                                <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div> 

                                      </div>
                                    </div>
                                 </div>

              <div class="col-md-12 col-lg-6">
                <div class="example-wrap">
                  <div class="example">
                      <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Assign DSNG:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                              

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Assigned NLE:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                  <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Assigned Vehicle:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                  <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div> 
                                    </div>
                                  </div>
               
              </div>
            </div>
          </div>
        </div>

            <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">
                 Reporter Panel
                </h3>
              </header>
              <div class="panel-body container-fluid">
                <div class="row row-lg">

                  <div class="col-md-12 col-lg-6" >      
                    <div class="example-wrap">
                      <div class="example">
                           <div class="form-group row">
                              <label class="col-md-3 col-form-label">Story No:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="date" disabled>
                                      </div>
                                    </div> 
                                  </div>

                                   <div class="form-group row">
                              <label class="col-md-3 col-form-label">Slug:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="date" disabled>
                                      </div>
                                    </div> 
                                  </div>

                                   <div class="form-group row">
                              <label class="col-md-3 col-form-label">Story:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <textarea class="form-control" name="story" rows="5" disabled >
                                      </textarea>
                                      </div>
                                    </div> 
                                  </div>

                                   <div class="form-group row">
                              <label class="col-md-3 col-form-label">Talking Heads:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="date" disabled>
                                      </div>
                                    </div> 
                                  </div>


                                  
                                    </div>
                                  </div>
                                </div>

                                  <div class="col-md-12 col-lg-6">
                                    <div class="example-wrap">
                                      <div class="example">
                                        <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Voice Over:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>


                                              <div class="form-group row">
                                                <label class="col-md-3 col-form-label">On Camera:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Political Party:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Time:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                            <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Date:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>
                                            
                                    </div>
                                  </div>
                                </div>


                                </div>
                                </div>
                                </div>



             <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">
                 Vehicle Panel
                </h3>
              </header>
              <div class="panel-body container-fluid">
                <div class="row row-lg">

                  <div class="col-md-12 col-lg-6" >      
                    <div class="example-wrap">
                      <div class="example">
                           <div class="form-group row">
                              <label class="col-md-3 col-form-label">Vehicle Leave Time:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="date" disabled>
                                      </div>
                                    </div> 
                                  </div>

                                  
                                    </div>
                                  </div>
                                </div>

                                  <div class="col-md-12 col-lg-6">
                                    <div class="example-wrap">
                                      <div class="example">
                                        <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Vehicle Arrived Time:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                            
                                    </div>
                                  </div>
                                </div>
                                </div>
                                </div>
                                </div>



        <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">
                  Store Panel 
                </h3>
              </header>
              <div class="panel-body container-fluid">
                <div class="row row-lg">

                  <div class="col-md-12 col-lg-6" >      
                    <div class="example-wrap">
                      <div class="example">
                           <div class="form-group row">
                              <label class="col-md-3 col-form-label">Equiment Delivered Time:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="date" disabled>
                                      </div>
                                    </div> 
                                  </div>
                              </div>
                            </div>
                          </div>

                                  <div class="col-md-12 col-lg-6">
                                    <div class="example-wrap">
                                      <div class="example">
                                        <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Equiment Received Time:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>      
                                       </div>
                                    </div>
                                </div>
                              </div>

      </div>
    </div>

    <div class="panel">
              <header class="panel-heading">
                <h3 class="panel-title">
                 NLE Panel
                </h3>
              </header>
              <div class="panel-body container-fluid">
                <div class="row row-lg">

                  <div class="col-md-12 col-lg-6" >      
                    <div class="example-wrap">
                      <div class="example">
                           <div class="form-group row">
                              <label class="col-md-3 col-form-label">Total No of Clips Received:</label>
                              <div class="col-md-9">
                                    <div class="example">
                                    <input type="text" class="form-control" name="date" disabled>
                                      </div>
                                    </div> 
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Grab Time Start:</label>
                                    <div class="col-md-9">
                                          <div class="example">
                                          <input type="text" class="form-control" name="date" disabled>
                                            </div>
                                          </div> 
                                        </div>
                                    </div>
                                  </div>
                                </div>

                                  <div class="col-md-12 col-lg-6">
                                    <div class="example-wrap">
                                      <div class="example">
                                        <div class="form-group row">
                                                <label class="col-md-3 col-form-label">Total Duration of Clips:</label>
                                                <div class="col-md-9">
                                              <div class="example">
                                                    <input type="text" class="form-control" name="date" disabled>
                                                </div>
                                              </div> 
                                            </div>

                                            <div class="form-group row">
                                    <label class="col-md-3 col-form-label">Grab Time End:</label>
                                    <div class="col-md-9">
                                          <div class="example">
                                          <input type="text" class="form-control" name="date" disabled>
                                            </div>
                                          </div> 
                                        </div>
                                    </div>
                                  </div>
                                </div>
                                </div>
                                </div>
                                </div>
                                 
                            



           
                          
   
    <!-- End Page Content -->
     </div>
@endsection
