@extends('admin')

  
@section('admincontent')
    <div class="page">
        <!-- Page Content -->
      <div class="page-content">

        <div class="panel">
                <div class="example-wrap">
                  <header class="panel-heading">
                <h3 class="panel-title">
                 Report Dispute 
                </h3>
              </header>
          <div class="panel-body container-fluid">
            <div class="row row-lg">
              <div class="col-md-9">
                <!-- Example Basic Form (Form grid) -->
                  
                  <div class="example">
                    <form action="{{url('test')}}" method="post">
                    {{ csrf_field() }}  
                    <div class="form-group row">
                                  <label class="col-md-3 col-form-label">Assignment Numer: </label>
                                  <div class="col-md-9">
                              <input type="text" class="form-control" name="Eventname" placeholder="Assignment No" >
                                  </div>
                            </div>
                      <div class="form-group row">
                        <label class="col-md-3 col-form-label">Event Name: </label>
                        <div class="col-md-9">
                          <input type="text" class="form-control" name="Eventname" placeholder="Event Name"  autocomplete="off"
                          />
                        </div>
                      </div>
                              
                              <div class="form-group row">
                                  <label class="col-md-3 col-form-label">Description: </label>
                                  <div class="col-md-9">
                                      <textarea class="form-control" placeholder="Add Some Description" rows="5" name="description"></textarea>
                                  </div>
                            </div>  

                        <div class="form-group row">
                        <label class="col-md-3 col-form-label"></label>
                        <div class="col-md-9">
                          <button type="submit" class="btn btn-block btn-primary">Submit </button>
                         <!--  <a href="{{url('reporter')}}" class="btn btn-block btn-danger">Back </a> -->
                        </div>
                      </div>                                                                      
                      </div>
                    </div>
                  </form>
                </div> 
            </div>
          </div>
        </div>

        
            
        
   
    <!-- End Page Content -->
     </div>
@endsection
