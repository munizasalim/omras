<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OMRAS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>OMRAS | Admin Login</title>
   
    
    <link rel="apple-touch-icon" href="{{asset('/admin/assets/base/assets/images/apple-touch-icon.png')}}" >
    <link rel="shortcut icon" href="{{asset('/admin/assets/base/assets/images/favicon.ico')}}">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/base/assets/css/site.min.css')}}">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/base/assets/examples/css/pages/login-v3.css')}}">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/web-icons/web-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    <script src="{{asset('/admin/assets/global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
      Breakpoints();
    </script>

</head>
<body class="animsition page-login-v3 layout-full">

    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
     
      <div class="page-content vertical-align-middle animation-slide-top animation-duration-1">
        
        <div class="panel">
          
          <div class="panel-body">
            
            <div class="brand">
              <div class="heading"><span>Sign In</span></div>
              <h2 class="brand-text font-size-18">Login to your Account</h2>
            </div>


            <form method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}">
                @csrf
                  
                  <div class="form-group form-material floating" data-plugin="formMaterial">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                
                        <label class="floating-label">{{ __('E-Mail Address') }}</label>
                    </div>
                        
                          <div class="form-group form-material floating" data-plugin="formMaterial">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                <label class="floating-label">{{ __('Password') }}</label>
                         </div>
              

                        <div class="form-group clearfix">
                            <div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                          
                          <label for="inputCheckbox">Remember me</label>
                      </div>

                                <a class="float-right" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                </div>
              
                            <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">{{ __('Login') }}</button>
            </form>
            
                 <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </div>  
        </div>

        <footer class="page-copyright page-copyright-inverse">
          <p> Copyrights © Pakistan Televison Corp. Ltd. All rights reserved.</p>
          
          <!-- <div class="social">
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-google-plus" aria-hidden="true"></i>
          </a>
          </div> -->
        </footer>
    </div>
</div>

 <script src="{{asset('/admin/assets/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/jquery/jquery.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/popper-js/umd/popper.min.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/bootstrap/bootstrap.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/animsition/animsition.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
    
    <!-- Plugins -->
    <script src="{{asset('/admin/assets/global/vendor/switchery/switchery.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/intro-js/intro.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/screenfull/screenfull.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
    
    <!-- Scripts -->
    <script src="{{asset('/admin/assets/global/js/Component.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Base.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Config.js')}}"></script>
    
    <script src="{{asset('/admin/assets/base/assets/js/Section/Menubar.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/GridMenu.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/Sidebar.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/PageAside.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Plugin/menu.js')}}"></script>
    
    <script src="{{asset('/admin/assets/global/js/config/colors.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/config/tour.js')}}"></script>
    <script>Config.set('assets', '../../assets');</script>
    
    <!-- Page -->
    <script src="{{asset('/admin/assets/base/assets/js/Site.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/asscrollable.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/slidepanel.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/switchery.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/jquery-placeholder.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/material.js')}}"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>

    
</body>
</html>






































