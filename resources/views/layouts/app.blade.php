<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'OMRAS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
    
    <title>OMRAS | Admin Login</title>

    
    <link rel="apple-touch-icon" href="{{asset('/admin/assets/base/assets/images/apple-touch-icon.png')}}" >
    <link rel="shortcut icon" href="{{asset('/admin/assets/base/assets/images/favicon.ico')}}">
    
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/css/bootstrap-extend.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/base/assets/css/site.min.css')}}">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/animsition/animsition.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/asscrollable/asScrollable.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/switchery/switchery.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/intro-js/introjs.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/slidepanel/slidePanel.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/vendor/flag-icon-css/flag-icon.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/base/assets/examples/css/pages/login-v3.css')}}">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/web-icons/web-icons.min.css')}}">
    <link rel="stylesheet" href="{{asset('/admin/assets/global/fonts/brand-icons/brand-icons.min.css')}}">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

    <!-- Scripts -->
    <script src="{{asset('/admin/assets/global/vendor/breakpoints/breakpoints.js')}}"></script>
    <script>
      Breakpoints();
    </script>

     <script src="{{asset('/admin/assets/global/vendor/babel-external-helpers/babel-external-helpers.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/jquery/jquery.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/popper-js/umd/popper.min.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/bootstrap/bootstrap.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/animsition/animsition.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/mousewheel/jquery.mousewheel.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/asscrollbar/jquery-asScrollbar.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/asscrollable/jquery-asScrollable.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/ashoverscroll/jquery-asHoverScroll.js')}}"></script>
    
    <!-- Plugins -->
    <script src="{{asset('/admin/assets/global/vendor/switchery/switchery.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/intro-js/intro.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/screenfull/screenfull.js')}}"></script>
    <script src="{{asset('/admin/assets/global/vendor/slidepanel/jquery-slidePanel.js')}}"></script>
        <script src="{{asset('/admin/assets/global/vendor/jquery-placeholder/jquery.placeholder.js')}}"></script>
    
    <!-- Scripts -->
    <script src="{{asset('/admin/assets/global/js/Component.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Base.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Config.js')}}"></script>
    
    <script src="{{asset('/admin/assets/base/assets/js/Section/Menubar.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/GridMenu.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/Sidebar.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Section/PageAside.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/Plugin/menu.js')}}"></script>
    
    <script src="{{asset('/admin/assets/global/js/config/colors.js')}}"></script>
    <script src="{{asset('/admin/assets/base/assets/js/config/tour.js')}}"></script>
    <script>Config.set('assets', '../../assets');</script>
    
    <!-- Page -->
    <script src="{{asset('/admin/assets/base/assets/js/Site.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/asscrollable.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/slidepanel.js')}}"></script>
    <script src="{{asset('/admin/assets/global/js/Plugin/switchery.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/jquery-placeholder.js')}}"></script>
        <script src="{{asset('/admin/assets/global/js/Plugin/material.js')}}"></script>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>




</head>
<body >
    <div id="app">
        

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
