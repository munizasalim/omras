<!DOCTYPE html>
<html lang="en" class="no-js">
<!-- BEGIN HEAD --> 
<head>
    @include('backend.include.head')
</head>
<body>
   <!-- BEGIN header -->   
    @include('backend.include.header')
   <!-- END header -->
    <div class="clearfix"></div>
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
      <!-- BEGIN SIDEBAR -->
      @include('backend.include.sidebarmenu')
      <!-- END SIDEBAR -->
      <!-- BEGIN PAGE -->
      <div class="page-container">
      @yield('admincontent')  
      <!-- END PAGE -->
      </div>
     <!-- END CONTAINER -->
     <!-- BEGIN FOOTER -->
     @include('backend.include.footer') 
      <!-- END FOOTER --> 
</body>
<!-- END BODY -->
</html>