<?php

use Illuminate\Support\Facades\Input;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Auth::routes();
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

//Route::get('/home', 'HomeController@index')->name('home');


Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/assignment', 'AssignmentController@index')->name('assignment');
Route::get('/addassignment', 'AssignmentController@addassignment')->name('addassignment');
Route::get('/viewassignment', 'AssignmentController@viewassignment')->name('viewassignment');
Route::get('/editassignment', 'AssignmentController@editassignment')->name('editassignment');

Route::get('/reporter', 'ReporterController@index')->name('reporter');
Route::get('/addcontentreporter', 'ReporterController@addcontent')->name('addcontentreporter');

Route::get('/cameraman', 'CameraManController@index')->name('cameraman');
Route::get('/chiefcameraman', 'ChiefCameraManController@index')->name('ChiefCamera');

Route::get('/reportdispute', 'AssignmentController@reportdispute')->name('reportdispute');

Route::get('/nleview', 'NleController@index')->name('nleview');
Route::get('/shiftinchargenle', 'NleController@shiftinchargenle')->name('shiftinchargenle');
Route::get('/UpdateNleStatus', 'NleController@UpdateStatus')->name('UpdateNleStatus');
Route::get('/NleMachineMaintenance', 'NleController@NleMachineMaintenance')->name('NleMachineMaintenance');


Route::get('/storekeeper', function () {
    return view('backend.storekeeper');
});

Route::get('/storemanagement', function () {
    return view('backend.storemanagement');
});


Route::get('/shiftinchargestore', function () {
    return view('backend.shiftinchargestore');
});
Route::get('/transportmanager', function () {
    return view('backend.transportmanager');
});

Route::get('/vehiclemanagement', function () {
    return view('backend.vehiclemanagement');
});


Route::get('/addhr', function () {
    return view('backend.addhr');
});

Route::get('/itsupport', function () {
    return view('backend.itsuppportengineer');
});

Route::get('/engineer', function () {
    return view('backend.engineerview');
});
Route::get('/chiefengineer', function () {
    return view('backend.chiefengineer');
});

Route::get('/maintenanceofequipment', function () {
    return view('backend.maintenanceofequipment');
});

Route::get('/driver', function () {
    return view('backend.driver');
});

Route::post('/test', function(){

			
			$input = Input::all();

			dd($input);


});








